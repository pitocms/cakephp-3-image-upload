<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AboutusImages Controller
 *
 * @property \App\Model\Table\AboutusImagesTable $AboutusImages
 */
class AboutusImagesController extends AppController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('aboutusImages', $this->paginate($this->AboutusImages));
        $this->set('_serialize', ['aboutusImages']);
    }

    /**
     * View method
     *
     * @param string|null $id Aboutus Image id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $aboutusImage = $this->AboutusImages->get($id, [
            'contain' => []
        ]);
        $this->set('aboutusImage', $aboutusImage);
        $this->set('_serialize', ['aboutusImage']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $aboutusImage = $this->AboutusImages->newEntity();
        if ($this->request->is('post')) {

            $image_name        = uniqid().$this->request->data['image']['name'];
            $image_tmp         = $this->request->data['image']['tmp_name'];
            $destination       = WWW_ROOT.'img'.DS.'aboutus'.DS.$image_name;
            move_uploaded_file($image_tmp, $destination);
            $this->request->data['image'] = $image_name;

            $aboutusImage = $this->AboutusImages->patchEntity($aboutusImage, $this->request->data);
            if ($this->AboutusImages->save($aboutusImage)) {
                $this->Flash->success(__('The aboutus image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The aboutus image could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('aboutusImage'));
        $this->set('_serialize', ['aboutusImage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Aboutus Image id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $aboutusImage = $this->AboutusImages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $aboutusImage = $this->AboutusImages->patchEntity($aboutusImage, $this->request->data);
            if ($this->AboutusImages->save($aboutusImage)) {
                $this->Flash->success(__('The aboutus image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The aboutus image could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('aboutusImage'));
        $this->set('_serialize', ['aboutusImage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Aboutus Image id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $aboutusImage = $this->AboutusImages->get($id);
        if ($this->AboutusImages->delete($aboutusImage)) {
            $this->Flash->success(__('The aboutus image has been deleted.'));
        } else {
            $this->Flash->error(__('The aboutus image could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
