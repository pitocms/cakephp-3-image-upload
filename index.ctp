<div class="row">
<div class="col-xs-12">
<div class="box">

    <div class="box-header">
        <h3 class="box-title"><?php echo __('About Us Images'); ?></h3>
        <div class="box-tools pull-right">
             <?= $this->Html->link(__('<span class="fa fa-plus"></span> New Aboutus Image'), ['action' => 'add'],['class'=>'btn btn-primary btn-sm','escape' => false]) ?>
        </div>
    </div><!-- /.box-header -->
     

<div class="box-body">
<div class="aboutusImages index large-9 medium-8 columns content">

    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('image') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
               
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aboutusImages as $aboutusImage): ?>
            <tr>
                <td><?= $this->Number->format($aboutusImage->id) ?></td>
                <td><?= $this->Html->image('aboutus/'.$aboutusImage->image,['class'=>'img-thumbnail','style'=>'max-width:90px;min-width:90px;']) ?></td>
                <td><?= h($aboutusImage->created) ?></td>
               
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $aboutusImage->id], ['class'=>'btn btn-danger btn-xs','confirm' => __('Are you sure you want to delete # {0}?', $aboutusImage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
</div>
</div>
</div>


