<div class="box box-primary">

     <div class="box-header with-border">
              <h3 class="box-title"><?= __('Add About Us image') ?></h3>
    </div><!-- /.box-header -->
<div class="box-body">
    <?= $this->Form->create($aboutusImage,array('type'=>'file')) ?>

     <div class="col-md-12">
        <?php
            echo $this->Form->input('image',['type' => 'file','class'=>'image-upload']);
        ?>
    </div>
     <div class="col-sm-12" style="margin-top:1%;">
        <button class="btn btn-primary btn-position">Submit</button>
   </div>
   <?= $this->Form->end() ?>
</div>
</div>

